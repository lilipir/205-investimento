package br.com.itau.investimento.model;

public class Resultado {
	
	private double valorFinal;

	public double getValorFinal() {
		return valorFinal;
	}

	public void setValorFinal(double valorFinal) {
		this.valorFinal = valorFinal;
	}

	
}
