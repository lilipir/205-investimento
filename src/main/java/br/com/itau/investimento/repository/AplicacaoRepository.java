package br.com.itau.investimento.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.investimento.model.Aplicacao;

@Repository
public interface AplicacaoRepository extends CrudRepository<Aplicacao, Long>{

}
