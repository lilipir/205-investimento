package br.com.itau.investimento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.model.Aplicacao;
import br.com.itau.investimento.model.Resultado;
import br.com.itau.investimento.service.AplicacaoService;

@RestController
public class AplicacaoController {
	
	@Autowired
	private AplicacaoService aplicacaoService;
	
	@GetMapping("/cliente/{id}/aplicacao")
	public Iterable<Aplicacao> listarAplicacao() {
		return aplicacaoService.obterAplicacao();
	}
	
	@PostMapping("/cliente/{id}/aplicacao")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarAplicacao(@PathVariable Long id, @RequestBody Aplicacao aplicacao) {
		aplicacaoService.criarAplicacao(id, aplicacao);
	}
	
	@PostMapping("/simulacao")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Resultado simularAplicacao(@RequestBody Aplicacao aplicacao) {
		return aplicacaoService.simularAplicacao(aplicacao);
	}
	
}
