package br.com.itau.investimento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.model.Cliente;
import br.com.itau.investimento.service.ClienteService;

@RestController
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping("/cliente")
	public Iterable<Cliente> listarCliente() {
		return clienteService.obterClientes();
	}
	
//	@GetMapping("/diretor/{id}/filmes")
//	public DiretorDTO listarFilmes(@PathVariable Long id) {
//		return diretorService.obterDiretor(id);
//	}
	
	@PostMapping("/cliente")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarCliente(@RequestBody Cliente cliente) {
		clienteService.criarCliente(cliente);
	}
	
	@DeleteMapping("/cliente/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarCliente(@PathVariable Long id) {
		clienteService.apagarCliente(id);
	}
	
	@PatchMapping("/cliente/{id}")
	public Cliente editarCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
		return clienteService.editarCliente(id, cliente);
	}
	
}
